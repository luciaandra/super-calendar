import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Calendar from './pages/Calendar';
import * as serviceWorker from './serviceWorker';

/* global document */
ReactDOM.render(<Calendar />, document.getElementById('root'));
serviceWorker.unregister();
