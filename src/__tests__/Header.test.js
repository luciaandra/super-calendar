import React from 'react';
import { render } from '@testing-library/react';
import Header from '../components/Header';
import items from '../viewItems';

import '@testing-library/jest-dom';

describe('Header', () => {
  it('it contains `Day`', () => {
    const { getByText } = render(<Header items={items} />);
    const dayString = getByText('Day');
    expect(dayString).toBeInTheDocument();
  });
});
