import moment from 'moment';

import { firstDayInWeek, isBetween } from '../utils/dates';

describe('Utils', () => {
  it('it returns the correct first day', () => {
    const day = firstDayInWeek(moment('2020-05-10'));
    expect(day).toBe('5');
  });

  it('it returns correct `between` date', () => {
    const isbetween = isBetween(moment('2020-05-10'), moment('2020-05-09'), moment('2020-05-11'));
    expect(isbetween).toBe(true);
  });

  it('it fails `between` date', () => {
    const isbetween = isBetween(moment('2020-05-10'), moment('2020-05-19'), moment('2020-05-21'));
    expect(isbetween).toBe(false);
  });
});
