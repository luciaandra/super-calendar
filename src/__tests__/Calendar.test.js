import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Calendar from '../pages/Calendar';

/* global document */

it('renders without crashing and matches snapshot', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Calendar />, div);
  ReactDOM.unmountComponentAtNode(div);
  const tree = renderer.create(<Calendar />).toJSON();
  expect(tree).toMatchSnapshot();
});
