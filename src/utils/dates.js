/**
 * Function returning the number of the day in the week on which the 1st day lands on
 *
 * @param {*} date
 */
export const firstDayInWeek = (date) => date.clone().startOf('month').format('d');

/**
 * Decides if the given day+month is between first or last days, or between the first and the hovered one
 *
 * @param {Moment} cellDay - Date of the current cell
 * @param {Moment} firstDay - First date selected
 * @param {Moment} lastDay - Last day selected
 * @param {Moment} hoverDay - Hovered date
 */
export const isBetween = (cellDay, firstDay, lastDay, hoverDay) =>
  cellDay.isBetween(firstDay, lastDay) || cellDay.isBetween(firstDay, hoverDay);
