import moment from 'moment';

// Variables to display the title in the header, depending on the selected view
const today = moment().format('dddd');
const thisWeek = `${moment().format('MM/DD')} - ${moment().add(7, 'days').format('MM/DD')}`;
const thisMonth = moment().format('MMMM');

// Items containing information about the view in the header
const items = [
  {
    name: 'Day',
    action: today,
    firstDay: moment(),
    lastDay: moment(),
  },
  {
    name: 'Week',
    action: thisWeek,
    firstDay: moment(),
    lastDay: moment().add(7, 'days'),
  },
  {
    name: 'Month',
    action: thisMonth,
    firstDay: moment().startOf('month'),
    lastDay: moment().endOf('month'),
  },
  {
    name: 'Range',
  },
];

export default items;
