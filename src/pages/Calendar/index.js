import React, { useState } from 'react';
import styled from 'styled-components';
import moment from 'moment';
import items from '../../viewItems';
import { color, device } from '../../themes';
import Header from '../../components/Header';
import CalendarView from '../../components/CalendarView';

const Wrapper = styled.div`
  background-color: ${color.black};
  color: ${color.white};
  font-family: sans-serif;
  padding: 14px;

  @media ${device.tablet} {
    padding: 30px 30px 50px 50px;
  }
`;

const HeaderWrapper = styled.div`
  align-items: flex-end;
  display: flex;
  flex-direction: column;

  @media ${device.tablet} {
    align-items: center;
    flex-direction: row;
  }
`;

const Title = styled.span`
  font-size: 28px;
  padding-bottom: 6px;
`;

const Calendar = () => {
  const [title, setTitle] = useState(moment().format('dddd'));
  const [activeView, setActiveView] = useState('Day');
  const [calendarRange, setCalendarRange] = useState({
    firstDay: moment(),
    lastDay: moment(),
    hoverDay: null,
  });

  const rangeTitle = (
    <>
      {calendarRange.firstDay.format('MM/DD/YYYY')}
      {calendarRange.lastDay &&
        ` - 
      ${calendarRange.lastDay.format('MM/DD/YYYY')}`}
    </>
  );

  return (
    <Wrapper>
      <HeaderWrapper>
        <Title>{activeView !== 'Range' ? title : rangeTitle}</Title>
        <Header
          items={items}
          activeView={activeView}
          handleActiveView={setActiveView}
          handleCalendarRange={(firstDay, lastDay) => setCalendarRange({ firstDay, lastDay })}
          handleTitle={setTitle}
        />
      </HeaderWrapper>
      {activeView === 'Range' && <CalendarView calendarRange={calendarRange} setCalendarRange={setCalendarRange} />}
    </Wrapper>
  );
};

export default Calendar;
