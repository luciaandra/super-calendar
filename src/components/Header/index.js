import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { color, device } from '../../themes';
import { ReactComponent as CalendarIcon } from '../../images/calendar.svg';

const Options = styled.ul`
  display: flex;
  font-size: 14px;
  list-style-type: none;
  padding: 0;

  @media ${device.tablet} {
    margin-left: auto;
  }
`;

const Item = styled.li`
  border-bottom: ${({ active }) => active && `1px solid ${color.red}`};
  color: ${({ active }) => (active ? `${color.red}` : `${color.grayLight}`)};
  cursor: pointer;
  display: flex;
  font-weight: 600;
  padding-bottom: 6px;
  text-transform: uppercase;

  &:not(:last-of-type) {
    margin-right: 26px;
  }
`;

const StyledCalendarIcon = styled(CalendarIcon)`
  margin-right: 6px;
  position: relative;

  path {
    fill: ${({ active }) => active && `${color.red}`};
  }
`;

const Header = ({ activeView, handleActiveView, handleCalendarRange, handleTitle, items }) => (
  <Options>
    {items.map(({ name, action, firstDay, lastDay }) => (
      <Item
        active={activeView === name}
        key={name}
        onClick={() => {
          handleActiveView(name);
          if (name !== 'Range') {
            handleTitle(action);
            handleCalendarRange(firstDay, lastDay);
          }
        }}
      >
        {name === 'Range' && <StyledCalendarIcon />}
        {name}
      </Item>
    ))}
  </Options>
);

Header.propTypes = {
  activeView: PropTypes.string,
  handleActiveView: PropTypes.func,
  handleCalendarRange: PropTypes.func,
  handleTitle: PropTypes.func,
  items: PropTypes.array,
};

export default Header;
