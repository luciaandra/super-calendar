import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { color } from '../../themes';
import { ReactComponent as CalendarIcon } from '../../images/calendar.svg';
import { ReactComponent as ArrowRight } from '../../images/arrow-right.svg';

const Wrapper = styled.div`
  align-items: center;
  background-color: ${color.grayDark};
  border-radius: 4px;
  color: ${color.grayLight};
  cursor: pointer;
  display: flex;
  font-size: 16px;
  padding: 14px 22px;
`;

const StyledCalendarIcon = styled(CalendarIcon)`
  margin-right: 18px;
`;

const StyledArrowRight = styled(ArrowRight)`
  height: 18px;
  margin-left: 18px;
  margin-right: 18px;
  width: 18px;
`;

const WeekInput = ({ calendarRange: { firstDay, lastDay } }) => (
  <Wrapper>
    <StyledCalendarIcon />
    {firstDay.format('MM/DD/YYYY')}
    <StyledArrowRight />
    {lastDay?.format('MM/DD/YYYY') || '--/--/----'}
  </Wrapper>
);

WeekInput.propTypes = {
  calendarRange: PropTypes.shape({
    firstDay: PropTypes.object,
    lastDay: PropTypes.object,
  }),
};

export default WeekInput;
