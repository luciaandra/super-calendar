import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { color } from '../../themes';
import { firstDayInWeek, isBetween } from '../../utils/dates';

const Table = styled.table`
  border-collapse: collapse;
`;

const StyledTh = styled.th`
  color: ${color.grayLight};
  font-size: 12px;
  font-weight: 400;
  height: 35px;
  max-height: 35px;
  max-width: 35px;
  width: 35px;
  text-transform: uppercase;
`;

const StyledTd = styled.td`
  background-color: ${({ isSelected, isbetween }) => {
    if (isSelected) return color.red;
    if (isbetween) return color.brown;
    return '';
  }};
  color: ${({ isSelected, isbetween, isCurrentDay }) =>
    isSelected || isbetween || isCurrentDay ? color.white : color.grayLight};
  cursor: ${({ isActive }) => (isActive ? 'pointer' : 'unset')};
  font-size: 12px;
  font-weight: ${({ isCurrentDay }) => (isCurrentDay ? 'bold' : 'normal')};
  height: 35px;
  max-height: 35px;
  max-width: 35px;
  text-align: center;
  width: 35px;

  > span {
    align-items: center;
    display: flex;
    justify-content: center;
  }

  &:hover {
    background-color: ${({ isActive }) => (isActive ? color.red : '')};
    color: ${({ isActive }) => (isActive ? color.white : '')};
  }
`;

const CalendarStructure = ({
  calendarRange: { firstDay, lastDay, hoverDay },
  handleDateSelect,
  handleHover,
  month,
  title,
}) => {
  // Array containing all week days with short naming
  const weekDaysShort = moment.weekdaysShort();

  // Array containing the empty table cells that do not display
  // any calendar day, depending on the value returned by `firstDayInWeek` function
  const emptyCell = [];
  for (let i = 0; i < firstDayInWeek(month); i += 1) {
    emptyCell.push(<StyledTd key={Math.random()} />);
  }

  // Function returning all days in the current month
  const currentMonthDays = () => {
    return month.daysInMonth();
  };

  // Array containing the populated table cells that display
  // calendar days contained by the current month
  const allDaysInMonth = [];
  for (let day = 1; day <= currentMonthDays(); day += 1) {
    const cellDay = month.clone().date(day);
    const isSelected = firstDay.isSame(cellDay, 'day') || lastDay?.isSame(cellDay, 'day');
    const isCurrentDay = cellDay.isSame(moment(), 'day');

    allDaysInMonth.push(
      <StyledTd
        key={cellDay.toString()}
        isSelected={isSelected}
        isHoverable={!lastDay}
        isbetween={isBetween(cellDay, firstDay, lastDay, hoverDay)}
        onClick={() => handleDateSelect(cellDay)}
        onMouseOver={() => handleHover(cellDay)}
        isCurrentDay={isCurrentDay}
        isActive
      >
        {day}
      </StyledTd>
    );
  }

  const allMonthCells = [...emptyCell, ...allDaysInMonth];
  const rows = [];
  let cells = [];

  // Create the calendar rows and cells based on all existing table cells (empty and with days)
  allMonthCells.forEach((row, i) => {
    if (i % 7 !== 0) {
      cells.push(row);
    } else {
      rows.push(cells);
      cells = [];
      cells.push(row);
    }
    if (i === allMonthCells.length - 1) {
      rows.push(cells);
    }
  });

  const monthCells = rows.map((row, index) => {
    // eslint-disable-next-line
    return <tr key={index}>{row}</tr>;
  });

  return (
    <div>
      {title}
      <Table>
        <thead>
          <tr>
            {weekDaysShort.map((day) => (
              <StyledTh key={day}>{day}</StyledTh>
            ))}
          </tr>
        </thead>
        <tbody>{monthCells}</tbody>
      </Table>
    </div>
  );
};

CalendarStructure.propTypes = {
  calendarRange: PropTypes.shape({
    firstDay: PropTypes.object,
    lastDay: PropTypes.object,
    hoverDay: PropTypes.object,
  }),
  handleDateSelect: PropTypes.func,
  handleHover: PropTypes.func,
  month: PropTypes.object,
  title: PropTypes.object,
};

export default CalendarStructure;
