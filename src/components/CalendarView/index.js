import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { color, device } from '../../themes';
import WeekInput from '../WeekInput';
import CalendarStructure from './CalendarStructure';
import { ReactComponent as ArrowLeftIcon } from '../../images/arrow-left.svg';
import { ReactComponent as ArrowRightIcon } from '../../images/arrow-right.svg';

const Wrapper = styled.div`
  align-items: flex-end;
  display: flex;
  flex-direction: column;
`;

const CalendarsWrapper = styled.div`
  background-color: ${color.grayDark};
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  margin-top: 20px;
  padding: 14px 20px 20px;

  > div:first-of-type {
    margin-right: 30px;
  }

  @media ${device.tablet} {
    flex-direction: row;
  }
`;

const TitleContent = styled.div`
  align-items: center;
  color: ${color.White};
  display: flex;
  font-size: 14px;
  font-weight: 600;
  padding: 10px 0;
  position: relative;
`;

const StyledArrowLeftIcon = styled(ArrowLeftIcon)`
  cursor: pointer;
  display: flex;
  flex: 0 1 auto;
`;

const StyledArrowRightIcon = styled(ArrowRightIcon)`
  cursor: pointer;
  display: flex;
  flex: 0 1 auto;
  margin-left: auto;
`;

const Title = styled.span`
  display: flex;
  flex: 0 1 auto;
  left: 50%;
  position: absolute;
  transform: translateX(-50%);
  text-transform: uppercase;
`;

const CalendarView = ({ calendarRange, setCalendarRange }) => {
  const [firstMonth, setFirstMonth] = useState(moment());
  const secondMonth = firstMonth.clone().add(1, 'months');

  // Condition selected dates in calendar
  const handleDateSelect = (selectedDay) => {
    const { firstDay, lastDay } = calendarRange;

    if ((firstDay && lastDay) || (firstDay && selectedDay.isBefore(firstDay))) {
      return setCalendarRange({
        firstDay: selectedDay,
        lastDay: null,
        hoverDay: null,
      });
    }

    return setCalendarRange({
      firstDay,
      lastDay: selectedDay,
      hoverDay: null,
    });
  };

  // Set the calendar hoverable cells
  const handleHover = (hoverDay) => {
    const { lastDay } = calendarRange;
    !lastDay &&
      setCalendarRange({
        ...calendarRange,
        hoverDay,
      });
  };

  // eslint-disable-next-line
  const TitleMonth = ({ firstMonthTitle, lastMonthTitle }) => (
    <TitleContent>
      {firstMonthTitle && (
        <StyledArrowLeftIcon
          onClick={() => {
            const mth = firstMonth.clone().subtract(1, 'months');
            setFirstMonth(mth);
          }}
        />
      )}
      <Title>{firstMonthTitle ? firstMonth.format('MMMM YYYY') : secondMonth.format('MMMM YYYY')}</Title>
      {lastMonthTitle && (
        <StyledArrowRightIcon
          onClick={() => {
            const mth = firstMonth.clone().add(1, 'months');
            setFirstMonth(mth);
          }}
        />
      )}
    </TitleContent>
  );

  return (
    <Wrapper>
      <WeekInput calendarRange={calendarRange} />
      <CalendarsWrapper>
        <CalendarStructure
          title={<TitleMonth firstMonthTitle />}
          calendarRange={calendarRange}
          month={firstMonth}
          handleDateSelect={handleDateSelect}
          handleHover={handleHover}
        />
        <CalendarStructure
          title={<TitleMonth lastMonthTitle />}
          calendarRange={calendarRange}
          month={secondMonth}
          handleDateSelect={handleDateSelect}
          handleHover={handleHover}
        />
      </CalendarsWrapper>
    </Wrapper>
  );
};

CalendarView.propTypes = {
  calendarRange: PropTypes.object,
  setCalendarRange: PropTypes.func,
};

export default CalendarView;
