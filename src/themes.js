export const color = {
  black: '#141414',
  brown: '#693d3b',
  grayLight: '#A1A1A1',
  grayDark: '#353535',
  red: '#F54141',
  white: '#FFFFFF',
};

export const device = {
  tablet: '(min-width: 768px)',
};
