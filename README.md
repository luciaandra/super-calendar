## Description

Created a Single Page Application showing a date range picker in React that supports both predefined ranges and custom selected ranges.

Added Prettier and ESLint for code format.

Added responsibe design.

## Tools used

ReactJS (create-react-app)
styled-components
prop-types
ESLint
momentJS

## Running the project

1. run `yarn`
2. run `yarn start`
3. In case the browser doesn't open automatically, you can go to `http://localhost:3000` to view the page
4. run `yarn test` to start the unit tests

## Remarks

1. For the 1 month range the requirements said that it should be "from the 1st of the current month until today". However, I don't think that this 'qualifies' as a month if, for example, we are in the first few days of the month. So I chose to set the entire current month as a range.
2. You can't select an end date before the start date. If you click a date that's in the past it will reset your start date to the one clicked. The same goes for hovering, it will not color the cells between what's selected and your mouse pointer if it's in the past
3. Added a mark on current day (bold and white), to be easier to see
